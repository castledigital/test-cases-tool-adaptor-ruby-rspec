# Attaching to project
#### Add following gems to your Gemfile

````ruby
# Gemfile
 
group :test do
  gem 'test-cases-tool-adaptor-ruby-rspec', :git => 'https://gitlab.com/castledigital/test-cases-tool-adaptor-ruby-rspec.git'
  gem 'test-cases-tool-builder-ruby', :git => 'https://gitlab.com/castledigital/test-cases-tool-builder-ruby.git'
end
````
#### Run `bundle install`
#### Require adaptor in `rails_helper`
````ruby
# spec/rails_helper.rb 
require 'test-cases-tool-adaptor-ruby-rspec'
````
#### Attach adaptor to RSpec
````ruby
# spec/rails_helper.rb 
  
module RspecInitializer
  extend self

  def init_rspec
    RSpec.configure do |config|
      config.include TestCasesToolRSpec::Adaptor, type: :feature
    end
  end
end
````
#### Configure adaptor and builder
````ruby
# spec/rails_helper.rb 

TestCasesToolRSpec.configure do |c|
  c.base_dir = "./spec/"
end
TestCasesToolBuilder.configure do |c|
  c.api_key = "your api key"
  c.url = "http://portal_host"
end
````
> * `base_dir` - a part of path that you won't to see in result tree (default: `./spec/`).
> * `api_key` - a sha key which you could get on portal. Also you could just set env var `TEST_CASES_TOOL_API_KEY` and leave the field empty.
> * `url` - a url to portal (default: http://ilikeyourmother.com)

### VCR
> If you use VCR for request specs, you should add portal endpoint to ignorance list

````ruby
# spec/support/vcr.rb
VCR.configure do |c|
  c.ignore_hosts 'http://portal_host'
end
````
