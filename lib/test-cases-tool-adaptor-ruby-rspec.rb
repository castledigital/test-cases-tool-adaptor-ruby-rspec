require 'test-cases-tool-adaptor-ruby-rspec/adaptor'
require 'test-cases-tool-adaptor-ruby-rspec/formatter'
require 'test-cases-tool-adaptor-ruby-rspec/steps'
require 'test-cases-tool-adaptor-ruby-rspec/preconditions'
require 'test-cases-tool-builder-ruby'

module TestCasesToolRSpec
  module Config
    class << self
      attr_accessor :base_dir

      def base_dir
        @base_dir || './spec/'
      end
    end
  end

  class Context
    attr_accessor :rspec

    def rspec
      @rspec
    end
  end

  class << self
    def context
      @context ||= Context.new
    end
  end

  class << self
    def configure(&block)
      yield Config
    end
  end
end
