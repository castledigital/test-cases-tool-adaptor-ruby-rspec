module TestCasesToolRSpec
  module Adaptor
    def self.included(base)
      TestCasesToolRSpec.context.rspec = base
      base.send :include, TestCasesToolRSpec::DSL
      if RSpec::Core::Formatters::Loader.formatters.keys.find_all { |f| f == TestCasesToolRSpec::Formatter }.empty?
        RSpec::Core::Formatters.register TestCasesToolRSpec::Formatter, *TestCasesToolRSpec::Formatter::NOTIFICATIONS
        RSpec.configuration.add_formatter(TestCasesToolRSpec::Formatter)
      end
      RSpec::Core::ExampleGroup.send :include, TestCasesToolRSpec::Precondition
    end
  end
end
