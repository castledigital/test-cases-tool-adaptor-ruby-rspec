require 'pry'
require 'rspec/core' unless defined?(RSpec::Core)
require 'rspec/core/formatters/progress_formatter' unless defined?(RSpec::Core::Formatters::ProgressFormatter)

module TestCasesToolRSpec

  class Formatter < RSpec::Core::Formatters::ProgressFormatter

    NOTIFICATIONS = [:example_started, :example_failed, :example_group_started, :example_passed, :example_pending, :start, :dump_summary]

    def example_failed(notification)
      super
      ## Doesn't work correct right now (all failed scenarios those failed not because of exception will be marked as broken)
      #status =  notification.example.execution_result.exception.is_a?(RSpec::Expectations::ExpectationNotMetError) ? :failed : :broken
      TestCasesToolBuilder::Builder.build_scenario(get_scenario_info(notification, :failed))
    end

    def example_group_started(notification)
      if notification.group.metadata[:type]
        if TestCasesToolBuilder::Config.types.include?(notification.group.metadata[:type].to_sym) || TestCasesToolBuilder::Config.types.empty?
          TestCasesToolBuilder::Builder.init_group_started
        end
      end
    end

    def example_passed(notification)
      super
      TestCasesToolBuilder::Builder.build_scenario(get_scenario_info(notification))
    end

    def example_pending(notification)
      super
      TestCasesToolBuilder::Builder.build_scenario(get_scenario_info(notification, :pending))
    end

    def example_started(notification)
      TestCasesToolBuilder::Builder.start_scenario
    end

    def start(example_count)
      super
      TestCasesToolBuilder::Builder.start_suite
    end

    def dump_summary(summary)
      super
      TestCasesToolBuilder::Builder.stop_suite(summary.failure_count > 0 ? :failed : :passed, summary.duration)
    end

    private

    def get_scenario_info(notification, status=:passed)
      {
          directory: directory(notification.example.example_group.metadata),
          scenario: notification.example.description,
          duration: notification.example.execution_result.run_time < 1 ? 1 : notification.example.execution_result.run_time,
          feature: nested_feature(notification.example.example_group.metadata).reject(&:blank?).reverse,
          status: status,
          code_block: notification.example.metadata[:block].source,
          location: notification.example.example_group.metadata[:location],
          exception: exception(notification.example.execution_result.exception)
      }
    end

    def directory(notification)
      if notification[:rerun_file_path].nil?
        notification[:file_path].sub(TestCasesToolRSpec::Config.base_dir, "").sub(/\/\w*.rb/, "").split("/")
      else
        notification[:rerun_file_path].sub(TestCasesToolRSpec::Config.base_dir, "").sub(/\/\w*.rb/, "").split("/")
      end
    end

    def exception(notification)
      unless notification.is_a?(NilClass)
        "#{notification.class.name}\n#{notification}"
      end
    end

    def nested_feature(data)
      features ||= []
      features.push(data[:description])
      if data[:parent_example_group]
        features << nested_feature(data[:parent_example_group])
      end
      features.flatten
    end
  end
end
