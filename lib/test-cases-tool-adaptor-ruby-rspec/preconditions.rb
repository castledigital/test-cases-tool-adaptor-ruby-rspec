module TestCasesToolRSpec
  module Precondition
    def self.included(base)
      base.class_eval do
        def self.precondition(precondition, &block)
          TestCasesToolBuilder::Builder.add_precondition(precondition)
          yield if block_given?
        end
      end
    end
  end
end
