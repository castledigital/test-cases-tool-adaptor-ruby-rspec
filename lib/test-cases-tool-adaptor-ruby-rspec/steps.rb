module TestCasesToolRSpec
  module DSL
    def step(step, &block)
      if block_given?
        begin
          yield
          status = :passed
        rescue => e
          raise e
        ensure
          TestCasesToolBuilder::Builder.step(step, status)
        end
      end
    end
    def precondition(precondition, &block)
      TestCasesToolBuilder::Builder.add_precondition(precondition)
      yield if block_given?
    end
  end
end

